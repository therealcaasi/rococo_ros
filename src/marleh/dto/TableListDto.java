package marleh.dto;


import java.util.List;
import java.util.ArrayList;
import marleh.dto.TableDto;
/**
 * Lists of Tables
 * @author Isaac Wahing
 * @version 0.01
 * Version History
 * [10/7/2017] 0.01 � Isaac Wahing � Initial codes.
 */
public class TableListDto extends BaseDto {
    /**
     * List of table dtos.
     */
    private List<TableDto> tables = new ArrayList<TableDto>();

    /**
     * Retrieve table list.
     * @return table list.
     */
    public List<TableDto> getEntries() {
        return tables;
    }

    /**
     * Sets table list.
     * @param tables the table list to set.
     */
    public void setEntries(List<TableDto> tables) {
        this.tables = tables;
    }
}
