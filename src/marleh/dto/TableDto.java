package marleh.dto;

/**
 * Contains the properties and methods for the table model.
 * @author Kent Caumeran
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Kent Caumeran  � Initial codes.
 */

public class TableDto extends BaseDto{
    private Long id;
    
    private String tableNumber;
    
    private String status;
    
    private int capacity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTableNumber() {
        return tableNumber;
    }

    public void setTableNumber(String tableNumber) {
        this.tableNumber = tableNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
