package marleh.dto;


import java.util.List;
import java.util.ArrayList;
import marleh.dto.OrderDto;
/**
 * List of Orders
 * @author Isaac Wahing
 * @version 0.01
 * Version History
 * [10/7/2017] 0.01 � Isaac Wahing � Initial codes.
 */
public class OrderListDto extends BaseDto {
    /**
     * List of order dtos.
     */
    private List<OrderDto> orders = new ArrayList<OrderDto>();

    /**
     * Retrieve order list.
     * @return order list.
     */
    public List<OrderDto> getEntries() {
        return orders;
    }

    /**
     * Sets order list.
     * @param orders the order list to set.
     */
    public void setEntries(List<OrderDto> orders) {
        this.orders = orders;
    }
}
