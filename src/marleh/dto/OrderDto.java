package marleh.dto;

import java.util.List;

/**
 * Contains the properties and methods for the order model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */

public class OrderDto extends BaseDto {
    
    /**
     *  Id of the Order.
     */
    private Long id;
    
    /**
     *  Total Cost of the Order.
     */
    private float totalCost;
    
    /**
    *   Status of the Order.
    */
    private String status;
    
    /**
    *   List of id's of 'MenuItem' in Order.
    */
    private List <Long> menuItems;
    private List <Long> quantities;
    /**
    *   TimeStamp of the Order.
    */
    private String timeStamp;
    private String orderNumber;
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Long> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<Long> menuItems) {
        this.menuItems = menuItems;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public List <Long> getQuantities() {
        return quantities;
    }

    public void setQuantities(List <Long> quantities) {
        this.quantities = quantities;
    }

    
    
}
