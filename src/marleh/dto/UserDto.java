package marleh.dto;

/**
 * Contains the properties and methods for the order model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [10/04/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */

public class UserDto extends BaseDto {
     /**
     *   Id of the User.
     */
     private Long id;
     /**
     *   Username of the User.
     */
     private String username;
     /**
     *   Password of the User.
     */
     private String password;
     
     public Long getId() {
         return id;
     }

     public void setId(Long id) {
         this.id = id;
     }
     
     public String getUsername() {
         return username;
     }

     public void setUsername(String username) {
         this.username = username;
     }
     
     public String getPassword() {
         return password;
     }

     public void setPassword(String password) {
         this.password = password;
     }
     
}
