/* ------------------------------------------------------------------------------
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Copyright (C) Rococo Global Technologies, Inc - All Rights Reserved 2016
 * --------------------------------------------------------------------------- */
package marleh.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Contains the DTO for the list of todo entity.
 * @author Glaiza T. Avila
 * @version 0.01
 * Version History
 * [03/11/2016] 0.01 � Glaiza T. Avila  � Initial codes.
 */
public class MenuItemListDto extends BaseDto {

    /**
     * List of menuItem dtos.
     */
    private List<MenuItemDto> menuItems = new ArrayList<MenuItemDto>();

    /**
     * Retrieve menuItem list.
     * @return menuItem list.
     */
    public List<MenuItemDto> getEntries() {
        return menuItems;
    }

    /**
     * Sets menuItem list.
     * @param menuItems the menuItem list to set.
     */
    public void setEntries(List<MenuItemDto> menuItems) {
        this.menuItems = menuItems;
    }
}