package marleh.model;

import java.io.Serializable;
import java.util.*;
import com.google.appengine.api.datastore.Key;

import org.slim3.datastore.Attribute;
import org.slim3.datastore.Model;

@Model(schemaVersion = 1)
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @Attribute(primaryKey = true)
    private Key key;

    @Attribute(version = true)
    private Long version;
    
    private Long id;
    
    private float totalCost;
    
    private String status;
    
    private List <Long> menuItems;
    private List <Long> quantities;
    
    private String timeStamp;
    
    private String orderNumber;
    

    /**
     * Returns the key.
     *
     * @return the key
     */
    public Key getKey() {
        return key;
    }

    /**
     * Sets the key.
     *
     * @param key
     *            the key
     */
    public void setKey(Key key) {
        this.key = key;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * Returns the version.
     *
     * @return the version
     */
    public Long getVersion() {
        return version;
    }

    /**
     * Sets the version.
     *
     * @param version
     *            the version
     */
    public void setVersion(Long version) {
        this.version = version;
    }
    
    /**
     * Returns the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets the totalCost.
     *
     * @param totalCost
     *            the totalCost
     */
    public void setId(Long id) {
        this.id = id;
    }
    
    /**
     * Returns the totalCost.
     *
     * @return the totalCost
     */
    public float getTotalCost() {
        return totalCost;
    }

    /**
     * Sets the totalCost.
     *
     * @param totalCost
     *            the totalCost
     */
    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }
    
    /**
     * Returns the totalCost.
     *
     * @return the totalCost
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the totalCost.
     *
     * @param totalCost
     *            the totalCost
     */
    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
    /**
     * Returns the List <MenuItem>.
     *
     * @return the List <MenuItem>
     */
    public List<Long> getMenuItems() {
        return menuItems;
    }

    /**
     * Sets the List <MenuItem>.
     *
     * @param List <MenuItem>
     *            the List <MenuItem>
     */
    public void setMenuItems(List <Long> menuItems) {
        this.menuItems = menuItems;
    }
    
    /**
     * Returns the timeStarted.
     *
     * @return the timeStarted
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Sets the totalCost.
     *
     * @param totalCost
     *            the totalCost
     */
    public void setTimeStamp(String timeStarted) {
        this.timeStamp = timeStarted;
    }
    
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Order other = (Order) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        return true;
    }

    public List <Long> getQuantities() {
        return quantities;
    }

    public void setQuantities(List <Long> quantities) {
        this.quantities = quantities;
    }
}
