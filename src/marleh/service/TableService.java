package marleh.service;

import java.util.List;

import marleh.common.GlobalConstants;
import marleh.dao.TableDao;
import marleh.dto.TableDto;
import marleh.dto.TableListDto;
import marleh.model.Table;

/**
 * Service for Table.
 * @author Kent Caumeran
 * @version 0.01
 * Version History
 * [09/22/2017] 0.01 � Kent Caumeran � Initial codes.
 */

public class TableService {
    TableDao tableDao = new TableDao();
    
    public TableListDto getTableList() {
        System.out.println("OrderListDto.getTodoList " + "start");
        // initializing the dto to hold the list of todos.
        TableListDto tableListDto =  new TableListDto();
        
        try {
            // get the list of todos with the given status. 
            List<Table> tableList = tableDao.getTableList();
            if (tableList != null) {
                // convert each todoModel from the todoList into TodoDto
                for (Table resultModel : tableList) {
                    TableDto tableDto = new TableDto();
                    
                    tableDto.setId(resultModel.getId());
                    tableDto.setCapacity(resultModel.getCapacity());
                    tableDto.setStatus(resultModel.getStatus());
                    tableDto.setTableNumber(resultModel.getTableNumber());

                    // adding the dto to the list
                    tableListDto.getEntries().add(tableDto);
                }
            }
        }catch(Exception e) {
            tableListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        System.out.println("OrderListDto.getTodoList " + "end");
        return tableListDto;
    }
    
    public void insertTable(TableDto inputTable){
        System.out.println("TableService.insertTable start");
        
        Table tableModel = new Table();
        
        tableModel.setCapacity(inputTable.getCapacity());
        tableModel.setStatus(inputTable.getStatus());
        tableModel.setTableNumber(inputTable.getTableNumber());
        
        try{
            if(this.tableDao.getTableByTableNumber(tableModel) != null){
                System.out.println("Table name is already taken");
            } else{
                tableDao.insertTable(tableModel);
            }
        } catch(Exception e){
            System.out.println("Exception in inserting Table"+e.toString());
        }
        
        System.out.println("TableService.insertTable end");
    }
    
    public void deleteTable(TableDto inputTable){
        System.out.println("TableService.deleteTable start");
        
        Table tableModel = new Table();
        tableModel.setId(inputTable.getId());
        
        try{
            Table resultModel = this.tableDao.getTableByTableNumber(tableModel);
            
            if(null == resultModel){
                System.out.println("Table does not exist");
            } else{
                try{
                    this.tableDao.deleteTable(resultModel);
                    System.out.println("Delete Table was successful");
                } catch(Exception e){
                    System.out.println("Exception in deleting table"+e.toString());
                }
            }
        } catch(Exception e){
            System.out.println("Exception in deleting table"+e.toString());
        }
        
        System.out.println("TableService.deleteTable end");
    }
    
    public void updateTable(TableDto inputTable){
        System.out.println("TableService.updateTable start");
        
        Table tableModel = new Table();
        
        tableModel.setCapacity(inputTable.getCapacity());
        tableModel.setStatus(inputTable.getStatus());
        tableModel.setTableNumber(inputTable.getTableNumber());
        
        try{
            if(this.tableDao.getTableByTableNumber(tableModel) == null){
                System.out.println("Table does not exist");
            } else{
                tableDao.insertTable(tableModel);
            }
        } catch(Exception e){
            System.out.println("Exception in updating Table "+e.toString());
        }
        
        System.out.println("TableService.updateTable start");
    }
    
    public TableDto retrieveTable(TableDto inputTable){
        
        Table tableModel = new Table();
        
        tableModel.setTableNumber(inputTable.getTableNumber());
        
        tableModel = tableDao.getTableByTableNumber(tableModel);
        
        inputTable.setCapacity(tableModel.getCapacity());
        
        return inputTable;
    }
    
}
