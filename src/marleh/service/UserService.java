package marleh.service;

import marleh.dao.UserDao;
import marleh.dto.UserDto;
import marleh.model.User;

/**
 * Contains the properties and methods for the User model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [10/07/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */

public class UserService {
UserDao UserDao = new UserDao();
    
    public void insertUser(UserDto inputUser){
        System.out.println("UserService.insertUser " + "start");
        
        User userModel = new User();
        
        userModel.setUsername(inputUser.getUsername());
        userModel.setPassword(inputUser.getPassword());
        
        try {

            if(this.UserDao.getUserByName(userModel) != null)
                System.out.println("User does exist");
            else
                UserDao.insertUser(userModel);

        } catch (Exception e) {
            inputUser.addError("Cannot add user");
            System.out.println("Exception in inserting bento: " + e.toString());
        }
        
        System.out.println("UserService.insertUser " + "end");
    }
    
    public void deleteUser(UserDto inputUser){
        System.out.println("UserService.deleteUser " + "start");
        
        User userModel = new User();
        userModel.setId(inputUser.getId());
        
        try {
            User resultModel = this.UserDao.getUserByName(userModel);

            if (null == resultModel) {
                // item doesn't exists
                System.out.println("Deleting bento. Item does not exists.");
            } else {
                // delete the item from the datastore
                try {
                    this.UserDao.deleteUser(resultModel);
                    System.out.println("-------Deleting User was successful.-------");
                } catch (Exception e) {
                    inputUser.addError("Cannot delete user");
                    System.out.println("Exception in deleting User: " + e.toString());
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in deleting User: " + e.toString());
        }
        
        System.out.println("UserService.deleteUser " + "end");
    }
    
    public void updateUser(UserDto inputUser){
        System.out.println("UserService.updateUser " + "start");
        
        User userModel = new User();

        userModel.setUsername(inputUser.getUsername());
        userModel.setPassword(inputUser.getPassword());
        
        try {

            if(this.UserDao.getUserByName(userModel) != null)
                UserDao.insertUser(userModel);
            else
                System.out.println("User does not exist");
        } catch (Exception e) {
            inputUser.addError("Cannot update user");
            System.out.println("Exception in inserting user: " + e.toString());
        }
        
        System.out.println("UserService.updateUser " + "end");
    }
}
