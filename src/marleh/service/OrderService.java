package marleh.service;


import java.util.List;

import marleh.common.GlobalConstants;
import marleh.dao.OrderDao;
import marleh.dto.OrderDto;
import marleh.dto.OrderListDto;
import marleh.model.Order;

/**
 * Service
 * @author Isaac Wahing
 * @version 0.01
 * Version History
 * [10/7/2017] 0.01 � Isaac Wahing � Initial codes.
 */

public class OrderService {
    

    OrderDao orderDao = new OrderDao();
    
    public void insertOrder(OrderDto inputOrder){
        System.out.println("OrderService.insertOrder " + "start");
        
        Order orderModel = new Order();
        
        orderModel.setOrderNumber(inputOrder.getOrderNumber());
        orderModel.setMenuItems(inputOrder.getMenuItems());
        orderModel.setQuantities(inputOrder.getQuantities());
        orderModel.setStatus(inputOrder.getStatus());
        orderModel.setTimeStamp(inputOrder.getTimeStamp());
        orderModel.setTotalCost(inputOrder.getTotalCost());
        
        try {

            if(this.orderDao.getOrderByOrderNumber(orderModel) != null)
                System.out.println("Order does exist");
            else
                orderDao.insertOrder(orderModel);
            
        } catch (Exception e) {
            inputOrder.addError("Cannot add order");
            System.out.println("Exception in inserting order: " + e.toString());
        }
        
        System.out.println("OrderService.insertOrder " + "end");
    }
    
    public void deleteOrder(OrderDto inputOrder){
        System.out.println("OrderService.deleteOrder " + "start");
        
        Order orderModel = new Order();
        orderModel.setOrderNumber(inputOrder.getOrderNumber());
        
        try {
            Order resultModel = this.orderDao.getOrderByOrderNumber(orderModel);

            if (null == resultModel) {
                // item doesn't exists
                System.out.println("Deleting order. Item does not exists.");
            } else {
                // delete the item from the datastore
                try {
                    this.orderDao.deleteOrder(resultModel);
                    System.out.println("-------Deleting order was successful.-------");
                } catch (Exception e) {
                    inputOrder.addError("Cannot delete order");
                    System.out.println("Exception in deleting order: " + e.toString());
                }
            }
        } catch (Exception e) {
            System.out.println("Exception in deleting order: " + e.toString());
        }
        
        System.out.println("OrderService.deleteOrder " + "end");
    }
    
    public void updateOrder(OrderDto inputOrder){
        System.out.println("OrderService.updateOrder " + "start");
        
        Order orderModel = new Order();
        orderModel.setOrderNumber(inputOrder.getOrderNumber());
        orderModel.setStatus(inputOrder.getStatus());
        orderModel.setMenuItems(inputOrder.getMenuItems());
        orderModel.setQuantities(inputOrder.getQuantities());
        orderModel.setTotalCost(inputOrder.getTotalCost());
        try {
            Order resultModel = orderDao.getOrderByOrderNumber(orderModel);
            
            System.out.println("UPDATE >>> Order Number:"+ orderModel.getOrderNumber()+" Status:"+orderModel.getStatus());
            if(resultModel != null){
                orderModel.setKey(resultModel.getKey());
                orderModel.setId(resultModel.getId());
                orderModel.setTimeStamp(resultModel.getTimeStamp());
                orderDao.updateOrder(orderModel);
            }
            else{
                System.out.println("Order does not exist");
            }

        } catch (Exception e) {
            inputOrder.addError("Cannot update order");
            System.out.println("Exception in inserting bento: " + e.toString());
        }
        
        System.out.println("OrderService.updateOrder " + "end");
    }
    
    public OrderListDto getOrderList() {
        System.out.println("OrderListDto.getTodoList " + "start");
        // initializing the dto to hold the list of todos.
        OrderListDto orderListDto =  new OrderListDto();
        
        try {
            // get the list of todos with the given status. 
            List<Order> orderList = orderDao.getPersonList();
            if (orderList != null) {
                // convert each todoModel from the todoList into TodoDto
                for (Order resultModel : orderList) {
                    OrderDto orderDto = new OrderDto();
                    
                    orderDto.setId(resultModel.getId());
                    orderDto.setOrderNumber(resultModel.getOrderNumber());
                    orderDto.setMenuItems(resultModel.getMenuItems());
                    orderDto.setQuantities(resultModel.getQuantities());
                    orderDto.setStatus(resultModel.getStatus());
                    orderDto.setTimeStamp(resultModel.getTimeStamp());
                    orderDto.setTotalCost(resultModel.getTotalCost());
                    
                    // adding the dto to the list
                    orderListDto.getEntries().add(orderDto);
                }
            }
        }catch(Exception e) {
            orderListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        System.out.println("OrderListDto.getTodoList " + "end");
        return orderListDto;
    }
}
