package marleh.service;

import java.util.List;

import marleh.common.GlobalConstants;
import marleh.dao.MenuItemDao;
import marleh.dto.MenuItemDto;
import marleh.dto.MenuItemListDto;
import marleh.model.MenuItem;

/**
 * Controller that will accessed when 'localhost:8888' is entered.
 * @author Glaiza t. Avila
 * @version 0.01
 * Version History
 * [09/22/2017] 0.01 � Glaiza T. Avila � Initial codes.
 */

public class MenuItemService {
    

    MenuItemDao menuItemDao = new MenuItemDao();
    
    public MenuItemDto insertMenuItem(MenuItemDto inputMenuItem){
        System.out.println("MenuItemService.insertMenuItem " + "start");
        
        MenuItem menuItemModel = new MenuItem();
        
        menuItemModel.setItemName(inputMenuItem.getItemName());
        menuItemModel.setNumberOfServings(inputMenuItem.getNumberOfServings());
        menuItemModel.setItemPrice(inputMenuItem.getItemPrice());
        menuItemModel.setAvailability(inputMenuItem.getAvailability());
        menuItemModel.setOrderCount(inputMenuItem.getOrderCount());
        
        Long newID = -1l;
        try {
            if(this.menuItemDao.getMenuItemByName(menuItemModel) != null) {
                System.out.println("MenuItem does exist");
            } else {
                newID = menuItemDao.insertMenuItem(menuItemModel);
            }
        } catch (Exception e) {
            System.out.println("Exception in inserting menu item: " + e.toString());
        }
        
        System.out.println("MenuItemService.insertMenuItem " + "end");
        
        inputMenuItem.setId(newID);
        
        return inputMenuItem;
    }
    
    public MenuItemDto deleteMenuItem(MenuItemDto inputMenuItem){
        System.out.println("MenuItemService.deleteMenuItem " + "start");
        
        MenuItem menuItemModel = new MenuItem();
        menuItemModel.setId(inputMenuItem.getId());
        
        try {
            MenuItem resultModel = this.menuItemDao.getMenuItemByID(menuItemModel);
            Long orderCount = resultModel.getOrderCount();
            
            if(orderCount == 0) {
                // delete the item from the datastore
                try {
                    this.menuItemDao.deleteMenuItem(resultModel);
                    System.out.println("-------Deleting MenuItem was successful.-------");
                } catch (Exception e) {
                    System.out.println("Exception in deleting MenuItem: " + e.toString());
                }
            } else {
                inputMenuItem.getErrorList().add("Menu Item has already been ordered.");
            }
        } catch (Exception e) {
            System.out.println("Exception in deleting MenuItem: " + e.toString());
        }
        
        System.out.println("MenuItemService.deleteMenuItem " + "end");
        
        return inputMenuItem;
    }
    
    public MenuItemDto updateMenuItem(MenuItemDto inputMenuItem){
        System.out.println("MenuItemService.updateMenuItem " + "start");
        
        MenuItem menuItemModel = new MenuItem();
        
        menuItemModel.setId(inputMenuItem.getId());
        menuItemModel.setItemName(inputMenuItem.getItemName());
        menuItemModel.setNumberOfServings(inputMenuItem.getNumberOfServings());
        menuItemModel.setItemPrice(inputMenuItem.getItemPrice());
        menuItemModel.setAvailability(inputMenuItem.getAvailability());
        
        try {
            MenuItem holder;
            if((holder = this.menuItemDao.getMenuItemByID(menuItemModel)) != null) {
                menuItemModel.setKey(holder.getKey());
                menuItemModel.setOrderCount(holder.getOrderCount());
                menuItemDao.updateMenuItem(menuItemModel);
            } else {
                System.out.println("MenuItem does not exist");
            }

        } catch (Exception e) {
            System.out.println("Exception in inserting bento: " + e.toString());
        }
        
        System.out.println("MenuItemService.updateMenuItem " + "end");
        
        return inputMenuItem;
    }
    
    
    public MenuItemListDto getMenuItemList() {
        System.out.println("MenuItemListDto.getMenuItemList " + "start");
        // initializing the dto to hold the list of menuItems.
        MenuItemListDto menuItemListDto =  new MenuItemListDto();
        
        try {
            // get the list of menuItems with the given status. 
            List<MenuItem> menuItemList = menuItemDao.getMenuItemList();
            if (menuItemList != null) {
                // convert each menuItem from the menuItemList into MenuItemDto
                for (MenuItem resultModel : menuItemList) {
                    MenuItemDto menuItemDto = new MenuItemDto();
                    
                    menuItemDto.setId(resultModel.getId());
                    menuItemDto.setItemName(resultModel.getItemName());
                    menuItemDto.setNumberOfServings(resultModel.getNumberOfServings());
                    menuItemDto.setItemPrice(resultModel.getItemPrice());
                    menuItemDto.setAvailability(resultModel.getAvailability());
                    
                    // adding the dto to the list
                    menuItemListDto.getEntries().add(menuItemDto);
                }
            }
        }catch(Exception e) {
            menuItemListDto.addError(GlobalConstants.ERR_ENTRY_NOT_FOUND);
        }
        System.out.println("PersonListDto.getTodoList " + "end");
        return menuItemListDto;
    }
}