package marleh.dao;

import java.util.ArrayList;
import java.util.List;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import marleh.meta.TableMeta;
import marleh.model.Table;

/**
 * Contains the properties and methods for the table model.
 * @author Kent Caumeran
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Kent Caumeran  � Initial codes.
 */

public class TableDao {
    
    public Table getTableByTableNumber(Table inputTable){
        System.out.println("TableDao.getTableById start");
        
        Table tableModel = new Table();
        TableMeta tableMeta = TableMeta.get();
        tableModel = Datastore.query(tableMeta)
                              .filter(tableMeta.tableNumber.equal(inputTable.getTableNumber()))
                              .asSingle();
        
        System.out.println("TableDao.getTableById end");
        return tableModel;
    }
    
    public List<Table> getTableList(){
        List<Table> tableModelList = new ArrayList<Table>();
        TableMeta meta = TableMeta.get();
        tableModelList = Datastore.query(meta)
                .asList();
        return tableModelList;
    }
    
    public void insertTable(Table inputTable){
        System.out.println("TableDao.insertTable start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        this.generateKeyAndId(inputTable);
    
        Datastore.put(inputTable);
        transaction.commit();
        
        System.out.println("TableDao.inserTable end");
    }
    
    public void updateTable(Table inputTable){
        System.out.println("TableDao.updateTable start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.put(inputTable);
        transaction.commit();
        
        System.out.println("TableDao.updateTable end");
    }
    
    public void deleteTable(Table inputTable){
        System.out.println("TableDao.deleteTable start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputTable.getKey());
        transaction.commit();
        
        System.out.println("TableDao.deleteTable end");
    }
    
    private void generateKeyAndId(Table inputTable) {
        Key parentKey = KeyFactory.createKey("Table", inputTable.getTableNumber());
        Key key = Datastore.allocateId(parentKey,"Table");
        
        inputTable.setKey(key);
        inputTable.setId(key.getId());
    }
}
