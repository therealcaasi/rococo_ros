package marleh.dao;

import marleh.meta.UserMeta;
import marleh.model.User;

import org.slim3.datastore.Datastore;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

/**
 * Contains the properties and methods for the User model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [10/07/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */

public class UserDao {
    
    public User getUserById(User inputUser){
        System.out.println("UserDao.getUserByName " + "start");
        User UserModel = new User();
        
        UserMeta userMeta = UserMeta.get();
        UserModel = Datastore.query(userMeta)
                              .filter(userMeta.id.equal(inputUser.getId()))
                              .asSingle();
        
        System.out.println("UserDao.getUserByName " + "end");
        return UserModel;
    }
    
    public User getUserByName(User inputUser){
        System.out.println("UserDao.getUserByName " + "start");
        User UserModel = new User();
        
        UserMeta userMeta = UserMeta.get();
        UserModel = Datastore.query(userMeta)
                              .filter(userMeta.username.equal(inputUser.getUsername()))
                              .asSingle();
        
        System.out.println("UserDao.getUserByName " + "end");
        return UserModel;
    }
    
    public void insertUser(User inputUser){
        System.out.println("UserDao.insertUser " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        this.generateKeyAndId(inputUser);
        
        Datastore.put(inputUser);
        transaction.commit();
        
        System.out.println("UserDao.insertUser " + "end");
    }
    
    public void updateUser(User inputUser){
        System.out.println("UserDao.updateUser " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.put(inputUser);
        transaction.commit();
        
        System.out.println("UserDao.updateUser " + "end");
    }
    
    public void deleteUser(User inputUser){
        System.out.println("UserDao.deleteUser " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputUser.getKey());
        transaction.commit();
        
        System.out.println("UserDao.deletUser " + "end");
    }
    
    private void generateKeyAndId(User inputUser) {
        Key parentKey = KeyFactory.createKey("User", inputUser.getUsername());
        Key key = Datastore.allocateId(parentKey,"User");

        inputUser.setKey(key);
        inputUser.setId(key.getId());
    }
    
}
