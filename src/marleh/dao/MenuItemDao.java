package marleh.dao;

import java.util.ArrayList;
import java.util.List;

import org.slim3.datastore.Datastore;

import marleh.meta.MenuItemMeta;
import marleh.model.MenuItem;


import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

/**
 * Contains the properties and methods for the menu item model.
 * @author Glaiza T. Avila
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Glaiza T. Avila  � Initial codes.
 */

public class MenuItemDao {
    
    public List<MenuItem> getMenuItemList(){
        List<MenuItem> menuItemList = new ArrayList<MenuItem>();
        MenuItemMeta meta = MenuItemMeta.get();
        menuItemList = Datastore.query(meta)
                .asList();
        return menuItemList;
    }
    
    public MenuItem getMenuItemByName(MenuItem inputMenuItem){
        System.out.println("MenuItemDao.getBentoByName " + "start");
        MenuItem menuItemModel = null;
        
        MenuItemMeta menuItemMeta = MenuItemMeta.get();
        menuItemModel = Datastore.query(menuItemMeta)
                              .filter(menuItemMeta.itemName.equal(inputMenuItem.getItemName()))
                              .asSingle();
        
        System.out.println("MenuItemDao.getBentoByName " + "end");
        return menuItemModel;
    }
    
    public MenuItem getMenuItemByID(MenuItem inputMenuItem) {
        System.out.println("MenuItemDao.getMenuItemByID " + "start");
        
        MenuItem menuItemModel = null;
        
        MenuItemMeta menuItemMeta = MenuItemMeta.get();
        menuItemModel = Datastore.query(menuItemMeta)
                                .filter(menuItemMeta.id.equal(inputMenuItem.getId()))
                                .asSingle();
        
        System.out.println("MenuItemDao.getMenuItemByID " + "end");
        
        return menuItemModel;
    }
    
    public Long insertMenuItem(MenuItem inputMenuItem){
        System.out.println("MenuItemDao.insertMenuItem " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        this.generateKeyAndId(inputMenuItem);
        
        Datastore.put(inputMenuItem);
        transaction.commit();
        
        System.out.println("MenuItemDao.insertMenuItem " + "end");
        
        return inputMenuItem.getId();
    }
    
    public void updateMenuItem(MenuItem inputMenuItem){
        System.out.println("MenuItemDao.updateMenuItem " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.put(inputMenuItem);
        transaction.commit();
        
        System.out.println("MenuItemDao.updateMenuItem " + "end");
    }
    
    public void deleteMenuItem(MenuItem inputMenuItem){
        System.out.println("MenuItemDao.updateMenuItem " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputMenuItem.getKey());
        transaction.commit();
        
        System.out.println("MenuItemDao.updateMenuItem " + "end");
    }
    
    private void generateKeyAndId(MenuItem inputMenuItem){
        Key parentKey = KeyFactory.createKey("MenuItem", inputMenuItem.getItemName());
        Key key = Datastore.allocateId(parentKey,"MenuItem");
        
        inputMenuItem.setKey(key);
        inputMenuItem.setId(key.getId());
    }
}