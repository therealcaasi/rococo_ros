package marleh.dao;

import java.util.List;
import java.util.ArrayList;

import org.slim3.datastore.Datastore;

import marleh.meta.OrderMeta;
import marleh.model.Order;


import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

/**
 * Contains the properties and methods for the order model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */

public class OrderDao {
    
    public Order getOrderByOrderNumber(Order inputOrder){
        System.out.println("OrderDao.getOrderByName " + "start");
        Order orderModel = new Order();
        
        OrderMeta orderMeta = OrderMeta.get();
        orderModel = Datastore.query(orderMeta)
                              .filter(orderMeta.orderNumber.equal(inputOrder.getOrderNumber()))
                              .asSingle();
        
        System.out.println("OrderDao.getOrderByNumber " + "end");
        return orderModel;
    }
    
    public List<Order> getPersonList(){
        System.out.println("OrderDao.getPersonList " + "start");
        List<Order> personModelList = new ArrayList<Order>();
        OrderMeta meta = OrderMeta.get();
        personModelList = Datastore.query(meta)
                .asList();

        System.out.println("OrderDao.getPersonList " + "end");
        return personModelList;
    }
    
    public void insertOrder(Order inputOrder){
        System.out.println("OrderDao.insertOrder " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        
        this.generateKeyAndId(inputOrder);
        
        Datastore.put(inputOrder);
        transaction.commit();
        
        System.out.println("OrderDao.insertOrder " + "end");
    }
    
    public void updateOrder(Order inputOrder){
        System.out.println("OrderDao.updateOrder " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        System.out.println("UPDATE >>> Order Number:"+ inputOrder.getOrderNumber()+" Status:"+inputOrder.getStatus());
        Datastore.put(inputOrder);
        transaction.commit();
        
        System.out.println("OrderDao.updateOrder " + "end");
    }
    
    public void deleteOrder(Order inputOrder){
        System.out.println("OrderDao.deleteOrder " + "start");
        
        Transaction transaction = Datastore.beginTransaction();
        Datastore.delete(inputOrder.getKey());
        transaction.commit();
        
        System.out.println("OrderDao.deleteOrder " + "end");
    }
    
    private void generateKeyAndId(Order inputOrder) {
        Key parentKey = KeyFactory.createKey("Order", inputOrder.getOrderNumber());
        Key key = Datastore.allocateId(parentKey,"Order");

        inputOrder.setKey(key);
        inputOrder.setId(key.getId());
    }
    
}
