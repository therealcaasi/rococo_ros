package marleh.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import marleh.service.OrderService;
import marleh.common.GlobalConstants;
import marleh.dto.OrderDto;
/**
 * Controller that will accessed when 'localhost:8888' is entered.
 * @author Isaac Wahing
 * @version 0.02
 * Version History
 * [10/07/2017] 0.01 � Isaac Wahing � Initial codes.
 */

public class RegisterOrderController extends Controller {
    
    OrderService orderService = new OrderService();
    OrderDto orderDto = new OrderDto();
    JSONObject json = null;
    
    @Override
    public Navigation run() throws Exception {
        System.out.println("RegisterOrderController.run " + "start");

        try{

            Validators validator = new Validators(this.request);
            
            validator.add("menuItems", validator.required());
            validator.add("status", validator.required());
            validator.add("timeStamp", validator.required());
            validator.add("totalCost", validator.required());
            validator.add("orderNumber", validator.required());
            validator.add("orderNumber", validator.integerType());
            validator.add("orderNumber", validator.maxlength(11));
            validator.add("orderNumber", validator.minlength(11));
            
            if(validator.validate()) {
                json = new JSONObject(new RequestMap(this.request));
                String menuItems = json.getString("menuItems");
                
                int index;
                String id, quantity;
                List<Long> menuItemsIDs = new ArrayList<Long>();
                List<Long> quantities = new ArrayList<Long>();
                System.out.println("dasdasdas = " + menuItems);
                while((index = menuItems.indexOf("\"ID\"")) != -1) {
                    int i;
                    for(i = index + 5; i < menuItems.length(); i++) {
                        if(menuItems.charAt(i) == ',') {
                            break;
                        }
                    }
                    id = menuItems.substring(index+5, i);
                    menuItemsIDs.add(Long.parseLong(id));
                    
                    menuItems = menuItems.substring(i);
                    
                    while((index = menuItems.indexOf("\"quantity\"")) != -1) {
                        int j;
                        for(j = index + 11; j < menuItems.length(); j++) {
                            if(menuItems.charAt(j) == ',') {
                                break;
                            }
                        }
                        quantity = menuItems.substring(index+11, j);
                        quantities.add(Long.parseLong(quantity));
                        
                        menuItems = menuItems.substring(j);
                        break;
                    }
                }

                String status = json.getString("status");
                String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new java.util.Date());
                float totalCost = Float.parseFloat(json.getString("totalCost"));
                String orderNumber = json.getString("orderNumber");
                
                orderDto.setOrderNumber(orderNumber);
                orderDto.setQuantities(quantities);
                orderDto.setMenuItems(menuItemsIDs);
                orderDto.setStatus(status);
                orderDto.setTimeStamp(timeStamp);
                orderDto.setTotalCost(totalCost);
                
                this.orderService.insertOrder(orderDto);
            }else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    orderDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
          if(orderDto.getErrorList().size() > 0) {
              json.put("errors", orderDto.getErrorList());
          }
          
        }catch(Exception e){
            System.out.println(e.toString());
            // Adds an error message if there exists.
            orderDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", orderDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());

        System.out.println("RegisterOrderController.run " + "end");
        return null;
    }
}
