package marleh.controller;

import marleh.common.GlobalConstants;
import marleh.dto.MenuItemDto;
import marleh.service.MenuItemService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

public class DeleteMenuItemController extends Controller {
    
    MenuItemService menuItemService = new MenuItemService();
    MenuItemDto menuItemDto = new MenuItemDto();
    JSONObject json = null;
    
    @Override
    public Navigation run() throws Exception {
        try {
            json = new JSONObject(new RequestMap(this.request));
            Long ID = json.getLong("ID");
            System.out.println("ID = " + ID);
            menuItemDto.setId(ID);
            menuItemService.deleteMenuItem(menuItemDto);
        } catch(Exception e) {
            System.out.println(e.toString());
            
            menuItemDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            
            if(json == null) {
                json = new JSONObject();
            }
        }
        
        json.put("errorList", menuItemDto.getErrorList());
        
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        
        response.getWriter().write(json.toString());
        
        
        return null;
    }
}
