package marleh.controller;


import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import marleh.service.OrderService;
import marleh.common.GlobalConstants;
import marleh.dto.OrderDto;

public class DeleteOrderController extends Controller {

    OrderService orderService = new OrderService();
    OrderDto orderDto = new OrderDto();
    JSONObject json = null;
    
    @Override
    public Navigation run() throws Exception {
        System.out.println("DeleteOrderController.run " + "start");
        
        try {
            json = new JSONObject(new RequestMap(this.request));
            String orderNumber = json.getString("orderNumber");

            orderDto.setOrderNumber(orderNumber);
            
            this.orderService.deleteOrder(orderDto);
        } catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            orderDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", orderDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        return null;
    }
}
