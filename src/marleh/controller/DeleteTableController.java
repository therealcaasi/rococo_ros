package marleh.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import marleh.common.GlobalConstants;
import marleh.dto.TableDto;
import marleh.service.TableService;
/**
 * Contains the properties and methods for the table model.
 * @author Kent Caumeran
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Kent Caumeran  � Initial codes.
 */

public class DeleteTableController extends Controller{

    TableService tableService = new TableService();
    TableDto tableDto = new TableDto();
    JSONObject json = null;
    
    @Override
    protected Navigation run() throws Exception {
        System.out.println("DeleteTableController.run start");
        
        TableDto tableDto = new TableDto();
        
        try{
            json = new JSONObject(this.request.getReader().readLine());
            Long id = json.getLong("id");
            
            tableDto.setId(id);
            
            this.tableService.deleteTable(tableDto);
        }catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            tableDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", tableDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());

        System.out.println("DeleteTableController.run end");
        
        return null;
        
    }
    
}
