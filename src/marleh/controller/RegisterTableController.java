package marleh.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

import marleh.common.GlobalConstants;
import marleh.dto.TableDto;
import marleh.service.TableService;
/**
 * Contains the properties and methods for the table model.
 * @author Kent Caumeran
 * @version 0.01
 * Version History
 * [09/27/2017] 0.01 � Kent Caumeran  � Initial codes.
 */

public class RegisterTableController extends Controller{

    TableService tableService = new TableService();
    TableDto tableDto = new TableDto();
    JSONObject json = null;

    @Override
    protected Navigation run() throws Exception {
        System.out.println("RegisterTableController.run " + "start");
        
        try{
            
            Validators validator = new Validators(this.request);
            
            //validator.add("id", validator.required());
            validator.add("status", validator.required());
            validator.add("capacity", validator.required());
            validator.add("tableNumber", validator.required());
            validator.add("tableNumber", validator.maxlength(2));
            validator.add("tableNumber", validator.minlength(2));
            validator.add("tableNumber", validator.integerType());
            
            if(validator.validate()) {
                json = new JSONObject(new RequestMap(this.request));
              
            String status = request.getParameter("status");
            int capacity = json.getInt("capacity");
            String tableNumber = json.getString("tableNumber");
            
            tableDto.setCapacity(capacity);
            tableDto.setStatus(status);
            tableDto.setTableNumber(tableNumber);
            
            this.tableService.insertTable(tableDto);
            }else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    tableDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
          if(tableDto.getErrorList().size() > 0) {
              json.put("errors", tableDto.getErrorList());
          }
          
        }catch(Exception e){
            System.out.println(e.toString());
            // Adds an error message if there exists.
            tableDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", tableDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());

        System.out.println("RegisterTableController.run " + "end");
        return null;
    }
}
