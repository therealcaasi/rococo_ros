package marleh.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

/**
 * Controller that will accessed when 'localhost:8888' is entered.
 * @author Isaac Wahing
 * @version 0.01
 * Version History
 * [09/22/2017] 0.01 � Isaac Wahing � Initial codes.
 */

public class IndexController extends Controller {
    /**
     * The function that will be ran first upon entering this controller.
     * Screen will transition to 'index.html' on admin/login folder.
     */
    @Override
    public Navigation run() throws Exception {
        return forward("admin/login");
    }
}
