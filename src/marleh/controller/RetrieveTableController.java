package marleh.controller;

import marleh.common.GlobalConstants;
import marleh.dto.TableDto;
import marleh.service.TableService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;

public class RetrieveTableController extends Controller {
    
    TableService tableService = new TableService();
    TableDto tableDto = new TableDto();
    JSONObject json = null;
    
    @Override
    public Navigation run() throws Exception {
        System.out.println("RetrieveTableController.run " + "start");
        
        try{
            Validators validator = new Validators(this.request);
            System.out.println(this.request.getParameter("tableNumber"));
            validator.add("tableNumber", validator.required());
            validator.add("tableNumber", validator.maxlength(2));
            validator.add("tableNumber", validator.minlength(2));
            validator.add("tableNumber", validator.integerType());
            
            if(validator.validate()) {
                json = new JSONObject(new RequestMap(this.request));
                    
                int tableNumber = json.getInt("tableNumber");
                System.out.println("HERE");
                tableDto.setTableNumber(tableNumber);
                System.out.println("HERE123");
                tableDto = tableService.retrieveTable(tableDto);
                System.out.println("HERE1234");
                json.put("tableCapacity", tableDto.getCapacity());
                System.out.println("HERE12345");
            }else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    tableDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
          if(tableDto.getErrorList().size() > 0) {
              json.put("errors", tableDto.getErrorList());
          }
        
        }catch(Exception e){
            System.out.println(e.toString());
            // Adds an error message if there exists.
            tableDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", tableDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());

        System.out.println("RegisterTableController.run " + "end");
        return null;
    }
}
