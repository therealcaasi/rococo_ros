package marleh.controller;

import marleh.common.GlobalConstants;
import marleh.dto.UserDto;
import marleh.service.UserService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.repackaged.org.json.JSONObject;
import org.slim3.util.RequestMap;
/**
 * Contains the properties and methods for the User model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [10/04/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */
public class RegisterUserController extends Controller {

    UserService UserService = new UserService();
    UserDto userDto = new UserDto();
    JSONObject json = null;
    
    @Override
    public Navigation run() throws Exception {
        System.out.println("RegisterUserController.run " + "start");
        
        
        try{

            Validators validator = new Validators(this.request);

            validator.add("username", validator.required());
            validator.add("password", validator.required());
            
            if(validator.validate()) {
                json = new JSONObject(new RequestMap(this.request));
                
                String username = request.getParameter("username");
                String password = request.getParameter("password");
                
                userDto.setUsername(username);
                userDto.setPassword(password);
                
                this.UserService.insertUser(userDto);
            }else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    userDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
          if(userDto.getErrorList().size() > 0) {
              json.put("errors", userDto.getErrorList());
          }
          
        }catch(Exception e){
            System.out.println(e.toString());
            // Adds an error message if there exists.
            userDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", userDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());

        System.out.println("RegisterUserController.run " + "end");
        return null;
    }
}
