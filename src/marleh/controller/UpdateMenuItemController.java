package marleh.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.controller.validator.Validators;
import org.slim3.util.RequestMap;

import com.google.appengine.labs.repackaged.org.json.JSONObject;

import marleh.service.MenuItemService;
import marleh.common.GlobalConstants;
import marleh.dto.MenuItemDto;
/**
 * Controller that will accessed when 'localhost:8888' is entered.
 * @author Glaiza T. Avila
 * @version 0.01
 * Version History
 * [09/22/2017] 0.01 � Glaiza T. Avila � Initial codes.
 */

public class UpdateMenuItemController extends Controller {
    

    MenuItemService menuItemService = new MenuItemService();
 
    @Override
    public Navigation run() throws Exception {
        System.out.println("UpdateMenuItemController.run " + "start");
        
        MenuItemDto menuItemDto = new MenuItemDto();
        JSONObject json = null;
        
        try{
            Validators validator = new Validators(this.request);
            
            validator.add("itemName", validator.required());
            validator.add("itemName", validator.minlength(1));
            validator.add("itemName", validator.maxlength(30));
            validator.add("numberOfServings", validator.required());
            validator.add("numberOfServings", validator.integerType());
            validator.add("itemPrice", validator.required());
            validator.add("availability", validator.required());
            
            String noOfServings = request.getParameter("numberOfServings");
            String availability = "Unavailable";
            
            if(!noOfServings.equals("0")){
                availability = "Available";
            }
            
            if(validator.validate()) {
                json = new JSONObject(new RequestMap(this.request));
                
                // add in to the datastore
                menuItemDto.setId(Long.parseLong(json.getString("ID")));
                menuItemDto.setItemName(json.getString("itemName"));
                menuItemDto.setNumberOfServings(Integer.parseInt(json.getString("numberOfServings")));
                menuItemDto.setItemPrice(Float.parseFloat(json.getString("itemPrice")));
                menuItemDto.setAvailability(availability);  
                
                menuItemService.updateMenuItem(menuItemDto);
            } else {
                for (int i = 0; i < validator.getErrors().size(); i++) {
                    menuItemDto.getErrorList().add(validator.getErrors().get(i));
                    System.out.println(validator.getErrors().get(i));
                }
            }
          if(menuItemDto.getErrorList().size() > 0) {
              json.put("errors", menuItemDto.getErrorList());
          }
        }catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            menuItemDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", menuItemDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());

        System.out.println("UpdateMenuItemController.run " + "end");
        
        return null;
    }
}
