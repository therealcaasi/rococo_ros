package marleh.controller;

import marleh.dto.UserDto;
import marleh.service.UserService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;
/**
 * Contains the properties and methods for the order model.
 * @author Isaac John Wahing
 * @version 0.01
 * Version History
 * [10/04/2017] 0.01 � Isaac John Wahing  � Initial codes.
 */
public class DeleteUserController extends Controller {

    UserService UserService = new UserService();
    UserDto userDto = new UserDto();
    JSONObject json = null;
    
    @Override
    public Navigation run() throws Exception {
        System.out.println("DeleteUserController.run " + "start");

        try{
            json = new JSONObject(this.request.getReader().readLine());
            String username = json.getString("userName");

            userDto.setUsername(username);
            
            this.UserService.deleteUser(userDto);
        }catch(Exception e){
            System.out.println("DeleteUserController.run.exception " + e.toString());
        }
        
        
        System.out.println("DeleteUserController.run " + "end");
        return forward("html/redirect.html/");
    }
}
