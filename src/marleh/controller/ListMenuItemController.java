package marleh.controller;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;
import org.slim3.repackaged.org.json.JSONObject;

import marleh.common.GlobalConstants;
import marleh.dto.MenuItemListDto;
import marleh.service.MenuItemService;



/**
 * Controller class used to get the list of todos.
 * @author Glaiza T. Avila
 * @version 0.01
 * Version History
 * [03/11/2016] 0.01 � Glaiza T. Avila  � Initial codes.
 */
public class ListMenuItemController extends Controller {
    
    /**
     * Instance of the TodoService that will be used to call the delete function.
     */
    private MenuItemService menuItemService = new MenuItemService(); 
    
    /**
     * The function that is ran when the controller is called.
     */
    @Override
    public Navigation run() throws Exception {
        System.out.println("ListMenuItemController.run " + "start");
        /**
         * Use to store the information that will be passed to the service.
         */
        MenuItemListDto menuItemListDto = new MenuItemListDto();
        /**
         * Holds the information to be passed in the response.
         */
        JSONObject json = null;
        try {
            json = new JSONObject();
            // getting the list of items
            menuItemListDto = menuItemService.getMenuItemList();
            // adding the list to the json that will be passed as response.
            json.put("menuItemList", menuItemListDto.getEntries());
            
        } catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            menuItemListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", menuItemListDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        
        System.out.println("ListMenuItemController.run " + "end");
        return null;
    }
}