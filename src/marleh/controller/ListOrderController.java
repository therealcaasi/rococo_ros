package marleh.controller;

import marleh.common.GlobalConstants;
import marleh.dto.OrderListDto;
import marleh.service.OrderService;

import org.slim3.controller.Controller;
import org.slim3.controller.Navigation;

import org.slim3.repackaged.org.json.JSONObject;

/**
 * Controller that will accessed when 'localhost:8888' is entered.
 * @author Isaac Wahing
 * @version 0.02
 * Version History
 * [10/07/2017] 0.01 � Isaac Wahing � Initial codes.
 */

public class ListOrderController extends Controller {
    

    private OrderService orderService = new OrderService(); 

    @Override
    public Navigation run() throws Exception {
        System.out.println("ListOrderController.run " + "start");
        
        OrderListDto orderListDto = new OrderListDto();
        /**
         * Holds the information to be passed in the response.
         */
        JSONObject json = null;
        try {
            json = new JSONObject();
            // getting the list of items
            orderListDto = orderService.getOrderList();
            // adding the list to the json that will be passed as response.
            json.put("orderList", orderListDto.getEntries());
            
        } catch (Exception e) {
            System.out.println(e.toString());
            // Adds an error message if there exists.
            orderListDto.addError(GlobalConstants.ERR_SERVER_CONTROLLER_PREFIX + e.getMessage());
            // initialize the json object that will be passed as response.
            if (null == json) {
                json = new JSONObject();
            }
        }
        // add the error message to the json object.
        json.put("errorList", orderListDto.getErrorList());
        
        // set the type of response.
        response.setContentType(GlobalConstants.SYS_CONTENT_TYPE_JSON);
        // send the response back to the JS file.
        response.getWriter().write(json.toString());
        
        System.out.println("ListOrderController.run " + "end");
        return null;
    }
}
