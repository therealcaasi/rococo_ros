package marleh.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2017-10-09 01:27:31")
/** */
public final class MenuItemMeta extends org.slim3.datastore.ModelMeta<marleh.model.MenuItem> {

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.MenuItem> availability = new org.slim3.datastore.StringAttributeMeta<marleh.model.MenuItem>(this, "availability", "availability");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.MenuItem> itemName = new org.slim3.datastore.StringAttributeMeta<marleh.model.MenuItem>(this, "itemName", "itemName");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Float> itemPrice = new org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Float>(this, "itemPrice", "itemPrice", float.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Integer> numberOfServings = new org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Integer>(this, "numberOfServings", "numberOfServings", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Long> orderCount = new org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Long>(this, "orderCount", "orderCount", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<marleh.model.MenuItem, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final MenuItemMeta slim3_singleton = new MenuItemMeta();

    /**
     * @return the singleton
     */
    public static MenuItemMeta get() {
       return slim3_singleton;
    }

    /** */
    public MenuItemMeta() {
        super("MenuItem", marleh.model.MenuItem.class);
    }

    @Override
    public marleh.model.MenuItem entityToModel(com.google.appengine.api.datastore.Entity entity) {
        marleh.model.MenuItem model = new marleh.model.MenuItem();
        model.setAvailability((java.lang.String) entity.getProperty("availability"));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setItemName((java.lang.String) entity.getProperty("itemName"));
        model.setItemPrice(doubleToPrimitiveFloat((java.lang.Double) entity.getProperty("itemPrice")));
        model.setKey(entity.getKey());
        model.setNumberOfServings(longToPrimitiveInt((java.lang.Long) entity.getProperty("numberOfServings")));
        model.setOrderCount((java.lang.Long) entity.getProperty("orderCount"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        marleh.model.MenuItem m = (marleh.model.MenuItem) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("availability", m.getAvailability());
        entity.setProperty("id", m.getId());
        entity.setProperty("itemName", m.getItemName());
        entity.setProperty("itemPrice", m.getItemPrice());
        entity.setProperty("numberOfServings", m.getNumberOfServings());
        entity.setProperty("orderCount", m.getOrderCount());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        marleh.model.MenuItem m = (marleh.model.MenuItem) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        marleh.model.MenuItem m = (marleh.model.MenuItem) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        marleh.model.MenuItem m = (marleh.model.MenuItem) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        marleh.model.MenuItem m = (marleh.model.MenuItem) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        marleh.model.MenuItem m = (marleh.model.MenuItem) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getAvailability() != null){
            writer.setNextPropertyName("availability");
            encoder0.encode(writer, m.getAvailability());
        }
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getItemName() != null){
            writer.setNextPropertyName("itemName");
            encoder0.encode(writer, m.getItemName());
        }
        writer.setNextPropertyName("itemPrice");
        encoder0.encode(writer, m.getItemPrice());
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        writer.setNextPropertyName("numberOfServings");
        encoder0.encode(writer, m.getNumberOfServings());
        if(m.getOrderCount() != null){
            writer.setNextPropertyName("orderCount");
            encoder0.encode(writer, m.getOrderCount());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected marleh.model.MenuItem jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        marleh.model.MenuItem m = new marleh.model.MenuItem();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("availability");
        m.setAvailability(decoder0.decode(reader, m.getAvailability()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("itemName");
        m.setItemName(decoder0.decode(reader, m.getItemName()));
        reader = rootReader.newObjectReader("itemPrice");
        m.setItemPrice(decoder0.decode(reader, m.getItemPrice()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("numberOfServings");
        m.setNumberOfServings(decoder0.decode(reader, m.getNumberOfServings()));
        reader = rootReader.newObjectReader("orderCount");
        m.setOrderCount(decoder0.decode(reader, m.getOrderCount()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}