package marleh.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2017-10-09 01:27:31")
/** */
public final class OrderMeta extends org.slim3.datastore.ModelMeta<marleh.model.Order> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<marleh.model.Order, java.util.List<java.lang.Long>, java.lang.Long> menuItems = new org.slim3.datastore.CollectionAttributeMeta<marleh.model.Order, java.util.List<java.lang.Long>, java.lang.Long>(this, "menuItems", "menuItems", java.util.List.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.Order> orderNumber = new org.slim3.datastore.StringAttributeMeta<marleh.model.Order>(this, "orderNumber", "orderNumber");

    /** */
    public final org.slim3.datastore.CollectionAttributeMeta<marleh.model.Order, java.util.List<java.lang.Long>, java.lang.Long> quantities = new org.slim3.datastore.CollectionAttributeMeta<marleh.model.Order, java.util.List<java.lang.Long>, java.lang.Long>(this, "quantities", "quantities", java.util.List.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.Order> status = new org.slim3.datastore.StringAttributeMeta<marleh.model.Order>(this, "status", "status");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.Order> timeStamp = new org.slim3.datastore.StringAttributeMeta<marleh.model.Order>(this, "timeStamp", "timeStamp");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, java.lang.Float> totalCost = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, java.lang.Float>(this, "totalCost", "totalCost", float.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Order, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final OrderMeta slim3_singleton = new OrderMeta();

    /**
     * @return the singleton
     */
    public static OrderMeta get() {
       return slim3_singleton;
    }

    /** */
    public OrderMeta() {
        super("Order", marleh.model.Order.class);
    }

    @Override
    public marleh.model.Order entityToModel(com.google.appengine.api.datastore.Entity entity) {
        marleh.model.Order model = new marleh.model.Order();
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setMenuItems(toList(java.lang.Long.class, entity.getProperty("menuItems")));
        model.setOrderNumber((java.lang.String) entity.getProperty("orderNumber"));
        model.setQuantities(toList(java.lang.Long.class, entity.getProperty("quantities")));
        model.setStatus((java.lang.String) entity.getProperty("status"));
        model.setTimeStamp((java.lang.String) entity.getProperty("timeStamp"));
        model.setTotalCost(doubleToPrimitiveFloat((java.lang.Double) entity.getProperty("totalCost")));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        marleh.model.Order m = (marleh.model.Order) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("id", m.getId());
        entity.setProperty("menuItems", m.getMenuItems());
        entity.setProperty("orderNumber", m.getOrderNumber());
        entity.setProperty("quantities", m.getQuantities());
        entity.setProperty("status", m.getStatus());
        entity.setProperty("timeStamp", m.getTimeStamp());
        entity.setProperty("totalCost", m.getTotalCost());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        marleh.model.Order m = (marleh.model.Order) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        marleh.model.Order m = (marleh.model.Order) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        marleh.model.Order m = (marleh.model.Order) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        marleh.model.Order m = (marleh.model.Order) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        marleh.model.Order m = (marleh.model.Order) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getMenuItems() != null){
            writer.setNextPropertyName("menuItems");
            writer.beginArray();
            for(java.lang.Long v : m.getMenuItems()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getOrderNumber() != null){
            writer.setNextPropertyName("orderNumber");
            encoder0.encode(writer, m.getOrderNumber());
        }
        if(m.getQuantities() != null){
            writer.setNextPropertyName("quantities");
            writer.beginArray();
            for(java.lang.Long v : m.getQuantities()){
                encoder0.encode(writer, v);
            }
            writer.endArray();
        }
        if(m.getStatus() != null){
            writer.setNextPropertyName("status");
            encoder0.encode(writer, m.getStatus());
        }
        if(m.getTimeStamp() != null){
            writer.setNextPropertyName("timeStamp");
            encoder0.encode(writer, m.getTimeStamp());
        }
        writer.setNextPropertyName("totalCost");
        encoder0.encode(writer, m.getTotalCost());
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected marleh.model.Order jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        marleh.model.Order m = new marleh.model.Order();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("menuItems");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("menuItems");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setMenuItems(elements);
            }
        }
        reader = rootReader.newObjectReader("orderNumber");
        m.setOrderNumber(decoder0.decode(reader, m.getOrderNumber()));
        reader = rootReader.newObjectReader("quantities");
        {
            java.util.ArrayList<java.lang.Long> elements = new java.util.ArrayList<java.lang.Long>();
            org.slim3.datastore.json.JsonArrayReader r = rootReader.newArrayReader("quantities");
            if(r != null){
                reader = r;
                int n = r.length();
                for(int i = 0; i < n; i++){
                    r.setIndex(i);
                    java.lang.Long v = decoder0.decode(reader, (java.lang.Long)null)                    ;
                    if(v != null){
                        elements.add(v);
                    }
                }
                m.setQuantities(elements);
            }
        }
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.getStatus()));
        reader = rootReader.newObjectReader("timeStamp");
        m.setTimeStamp(decoder0.decode(reader, m.getTimeStamp()));
        reader = rootReader.newObjectReader("totalCost");
        m.setTotalCost(decoder0.decode(reader, m.getTotalCost()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}