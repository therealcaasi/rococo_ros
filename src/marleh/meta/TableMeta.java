package marleh.meta;

//@javax.annotation.Generated(value = { "slim3-gen", "@VERSION@" }, date = "2017-10-09 00:47:24")

/** */
public final class TableMeta extends org.slim3.datastore.ModelMeta<marleh.model.Table> {

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, java.lang.Integer> capacity = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, java.lang.Integer>(this, "capacity", "capacity", int.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, java.lang.Long> id = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, java.lang.Long>(this, "id", "id", java.lang.Long.class);

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, com.google.appengine.api.datastore.Key> key = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, com.google.appengine.api.datastore.Key>(this, "__key__", "key", com.google.appengine.api.datastore.Key.class);

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.Table> status = new org.slim3.datastore.StringAttributeMeta<marleh.model.Table>(this, "status", "status");

    /** */
    public final org.slim3.datastore.StringAttributeMeta<marleh.model.Table> tableNumber = new org.slim3.datastore.StringAttributeMeta<marleh.model.Table>(this, "tableNumber", "tableNumber");

    /** */
    public final org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, java.lang.Long> version = new org.slim3.datastore.CoreAttributeMeta<marleh.model.Table, java.lang.Long>(this, "version", "version", java.lang.Long.class);

    private static final TableMeta slim3_singleton = new TableMeta();

    /**
     * @return the singleton
     */
    public static TableMeta get() {
       return slim3_singleton;
    }

    /** */
    public TableMeta() {
        super("Table", marleh.model.Table.class);
    }

    @Override
    public marleh.model.Table entityToModel(com.google.appengine.api.datastore.Entity entity) {
        marleh.model.Table model = new marleh.model.Table();
        model.setCapacity(longToPrimitiveInt((java.lang.Long) entity.getProperty("capacity")));
        model.setId((java.lang.Long) entity.getProperty("id"));
        model.setKey(entity.getKey());
        model.setStatus((java.lang.String) entity.getProperty("status"));
        model.setTableNumber((java.lang.String) entity.getProperty("tableNumber"));
        model.setVersion((java.lang.Long) entity.getProperty("version"));
        return model;
    }

    @Override
    public com.google.appengine.api.datastore.Entity modelToEntity(java.lang.Object model) {
        marleh.model.Table m = (marleh.model.Table) model;
        com.google.appengine.api.datastore.Entity entity = null;
        if (m.getKey() != null) {
            entity = new com.google.appengine.api.datastore.Entity(m.getKey());
        } else {
            entity = new com.google.appengine.api.datastore.Entity(kind);
        }
        entity.setProperty("capacity", m.getCapacity());
        entity.setProperty("id", m.getId());
        entity.setProperty("status", m.getStatus());
        entity.setProperty("tableNumber", m.getTableNumber());
        entity.setProperty("version", m.getVersion());
        entity.setProperty("slim3.schemaVersion", 1);
        return entity;
    }

    @Override
    protected com.google.appengine.api.datastore.Key getKey(Object model) {
        marleh.model.Table m = (marleh.model.Table) model;
        return m.getKey();
    }

    @Override
    protected void setKey(Object model, com.google.appengine.api.datastore.Key key) {
        validateKey(key);
        marleh.model.Table m = (marleh.model.Table) model;
        m.setKey(key);
    }

    @Override
    protected long getVersion(Object model) {
        marleh.model.Table m = (marleh.model.Table) model;
        return m.getVersion() != null ? m.getVersion().longValue() : 0L;
    }

    @Override
    protected void assignKeyToModelRefIfNecessary(com.google.appengine.api.datastore.AsyncDatastoreService ds, java.lang.Object model) {
    }

    @Override
    protected void incrementVersion(Object model) {
        marleh.model.Table m = (marleh.model.Table) model;
        long version = m.getVersion() != null ? m.getVersion().longValue() : 0L;
        m.setVersion(Long.valueOf(version + 1L));
    }

    @Override
    protected void prePut(Object model) {
    }

    @Override
    protected void postGet(Object model) {
    }

    @Override
    public String getSchemaVersionName() {
        return "slim3.schemaVersion";
    }

    @Override
    public String getClassHierarchyListName() {
        return "slim3.classHierarchyList";
    }

    @Override
    protected boolean isCipherProperty(String propertyName) {
        return false;
    }

    @Override
    protected void modelToJson(org.slim3.datastore.json.JsonWriter writer, java.lang.Object model, int maxDepth, int currentDepth) {
        marleh.model.Table m = (marleh.model.Table) model;
        writer.beginObject();
        org.slim3.datastore.json.Default encoder0 = new org.slim3.datastore.json.Default();
        writer.setNextPropertyName("capacity");
        encoder0.encode(writer, m.getCapacity());
        if(m.getId() != null){
            writer.setNextPropertyName("id");
            encoder0.encode(writer, m.getId());
        }
        if(m.getKey() != null){
            writer.setNextPropertyName("key");
            encoder0.encode(writer, m.getKey());
        }
        if(m.getStatus() != null){
            writer.setNextPropertyName("status");
            encoder0.encode(writer, m.getStatus());
        }
        if(m.getTableNumber() != null){
            writer.setNextPropertyName("tableNumber");
            encoder0.encode(writer, m.getTableNumber());
        }
        if(m.getVersion() != null){
            writer.setNextPropertyName("version");
            encoder0.encode(writer, m.getVersion());
        }
        writer.endObject();
    }

    @Override
    protected marleh.model.Table jsonToModel(org.slim3.datastore.json.JsonRootReader rootReader, int maxDepth, int currentDepth) {
        marleh.model.Table m = new marleh.model.Table();
        org.slim3.datastore.json.JsonReader reader = null;
        org.slim3.datastore.json.Default decoder0 = new org.slim3.datastore.json.Default();
        reader = rootReader.newObjectReader("capacity");
        m.setCapacity(decoder0.decode(reader, m.getCapacity()));
        reader = rootReader.newObjectReader("id");
        m.setId(decoder0.decode(reader, m.getId()));
        reader = rootReader.newObjectReader("key");
        m.setKey(decoder0.decode(reader, m.getKey()));
        reader = rootReader.newObjectReader("status");
        m.setStatus(decoder0.decode(reader, m.getStatus()));
        reader = rootReader.newObjectReader("tableNumber");
        m.setTableNumber(decoder0.decode(reader, m.getTableNumber()));
        reader = rootReader.newObjectReader("version");
        m.setVersion(decoder0.decode(reader, m.getVersion()));
        return m;
    }
}