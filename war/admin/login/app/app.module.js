var app = angular.module('myApp', []);
app.controller('myController', function($scope,$location,$window,$http,$httpParamSerializer) {
	
	$scope.verifyUser = function(){
		var jsonData = {
				"username": $scope.username,
				"password": $scope.password
		}
		$http.post("/Login", $httpParamSerializer(jsonData),
				{
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			)
			.then(function(response) {
				
				if (response.data.loginRequest==true){
					$window.location.href = '/admin/admin-panel';
					alert("Access Granted");
				}else{
					alert("Access Denied");
				}
			}, function() {
				alert("An error has occured");
			});
		
	}
	/*
	* TODO:
	*	1. Filter form inputs here
	*	2. Submit form inputs to server
	*/
});