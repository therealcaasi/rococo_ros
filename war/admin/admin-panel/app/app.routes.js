app.config(function($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: 'app/components/dashboard/dashboardView.html',
      controller: 'dashboardController'
    })
    .when("/menu-items", {
      templateUrl: 'app/components/menu-items/menuItemsView.html',
      controller: 'menuItemsController'
    })
    .when("/orders", {
      templateUrl: 'app/components/orders/ordersView.html',
      controller: 'ordersController'
    })
    .when("/tables", {
      templateUrl: 'app/components/tables/tablesView.html',
      controller: 'tablesController'
    });
});