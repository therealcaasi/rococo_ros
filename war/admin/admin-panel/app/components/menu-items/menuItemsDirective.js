app.directive('menuItemsDirective', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'app/components/menu-items/menuItemsView.html'
	};
});