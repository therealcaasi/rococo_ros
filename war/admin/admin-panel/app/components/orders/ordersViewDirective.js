app.directive('orders', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'app/components/orders/ordersView.html'
	};
});