app.directive('adminSidebar', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'app/components/sidebar/sidebarView.html'
	};
});