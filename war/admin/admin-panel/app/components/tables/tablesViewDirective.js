app.directive('tables', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'app/components/tables/tablesView.html'
	};
});