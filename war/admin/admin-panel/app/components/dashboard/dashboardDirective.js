app.directive('adminDashboard', function() {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'app/components/dashboard/dashboardView.html'
	};
});