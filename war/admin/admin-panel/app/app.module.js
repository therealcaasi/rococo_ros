var app = angular.module('myApp', ['ngRoute']);
app.controller('myController', function($scope) {
	$scope.dashboardActive = '';
	$scope.menuItemsActive = '';
	$scope.ordersActive = '';
	$scope.tablesActive = '';

	$scope.changeActiveMenuItem = function(menu_item) {
		switch(menu_item) {
			case 'dashboard':
				$scope.dashboardActive = 'active';
				$scope.menuItemsActive = '';
				$scope.ordersActive = '';
				$scope.tablesActive = '';
				break;
			case 'menu-items':
				$scope.dashboardActive = '';
				$scope.menuItemsActive = 'active';
				$scope.ordersActive = '';
				$scope.tablesActive = '';
				break;
			case 'orders':
				$scope.dashboardActive = '';
				$scope.menuItemsActive = '';
				$scope.ordersActive = 'active';
				$scope.tablesActive = '';
				break;
			case 'tables':
				$scope.dashboardActive = '';
				$scope.menuItemsActive = '';
				$scope.ordersActive = '';
				$scope.tablesActive = 'active';
				break;
		}
	};
});

app.controller('dashboardController', function($scope) {
	$scope.pageTitle = "Dashboard";
	$scope.$parent.dashboardActive = "active";
});

app.controller('menuItemsController', function($scope, $http, $httpParamSerializer) {
	$scope.pageTitle = "Menu Items";
	$scope.$parent.menuItemsActive = "active";
	$scope.menuItems = [];
	
	$http.get("/ListMenuItem").then(function(response) {
		if (response.data.errorList.length == 0) {
			//There were no errors.
			$scope.menuItems = response.data.menuItemList;
		} else {
			// display the error messages.
			var errorMessage = "";
			for (var i = 0; i < response.data.errorList.length; i++) {
				errorMessage += response.data.errorList[i];
			}
			alert(errorMessage);
		}
	}, function() {
		alert("An error has occured");
	});

	$scope.itemName = '';
	$scope.availability = 'All';
	$scope.filterData = function(menuItem) {
		if($scope.availability == 'All') {
			return menuItem.itemName.toLowerCase().includes($scope.itemName.toLowerCase());
		} else {
			return menuItem.availability == $scope.availability &&
					menuItem.itemName.toLowerCase().includes($scope.itemName.toLowerCase());
		}
	};

	
	$scope.editItem = function(repeat$scope) {
		repeat$scope.mode = 'edit';
	};

	$scope.deleteItem = function(itemIndex, repeat$scope) {
		var retVal = confirm("Are you sure you want to delete the item ?");
		if( retVal == true ){
			console.log("User deleted");
			$scope.menuItems.splice(itemIndex, 1);
			
			var json = {
				"ID": repeat$scope.menuItem.id
			};
			alert(repeat$scope.menuItem.itemName);
			
			$http.post("/DeleteMenuItem", $httpParamSerializer(json),
				{
				headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			)
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					//There were no errors.
					alert("Successfully Deleted Item");
				} else {
					// display the error messages.
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i];
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
		} else{
			console.log("User did not delete");
			return false;
		}
	};

	$scope.saveItem = function(repeat$scope) {
		var retVal = confirm("Are you sure you want to update the item ?");
		if( retVal == true ){
			repeat$scope.mode = '';
			console.log("User updated");
		
			if(isNaN(repeat$scope.menuItem.numberOfServings)) {
				alert('Number of servings must be an integer');
			} else {
				if(isNaN(repeat$scope.menuItem.itemPrice)) {
					alert('Price must be an integer');
				} else {
					if(repeat$scope.menuItem.numberOfServings > 0) {
						repeat$scope.menuItem.availability = "Available";
					} else {
						repeat$scope.menuItem.availability = "Unavailable";
					}
					repeat$scope.mode = '';
					
					var json = {
						"ID": repeat$scope.menuItem.id,
						"itemName": repeat$scope.menuItem.itemName,
						"numberOfServings": repeat$scope.menuItem.numberOfServings,
						"itemPrice": repeat$scope.menuItem.itemPrice
					};
					
					$http.post("/UpdateMenuItem", $httpParamSerializer(json),
						{
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						}
					)
					.then(function(response) {
						if (response.data.errorList.length == 0) {
							//There were no errors.
							alert("Successfully Updated Item");
							
						} else {
							// display the error messages.
							var errorMessage = "";
							for (var i = 0; i < response.data.errorList.length; i++) {
								errorMessage += response.data.errorList[i];
							}
							alert(errorMessage);
						}
					}, function() {
						alert("An error has occured");
					});
				}
			}
		} else{
			repeat$scope.mode = '';
			window.location.reload();
			console.log("User did not update");
			return false;
		}
	}

	$scope.createMenuItem = function() {
		
		if($scope.newItemName.length <= 30){
			if($scope.newNumberOfServings >= 0){
				newMenuItem = {
						"itemName": $scope.newItemName, 
						"numberOfServings": $scope.newNumberOfServings,
						"itemPrice": $scope.newItemPrice,
						"availability": ($scope.newNumberOfServings == 0 ? "Unavailable" : "Available")
					};
				
				$http.post("/RegisterMenuItem", $httpParamSerializer(newMenuItem),
						{// configuring the request not a JSON type.
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						}
				)
				.then(function(response) {
					if (response.data.errorList.length == 0) {
						//There were no errors.
						//$scope.clearAllFields();
						alert("Successfully Added Item");
						// updating the table
						newMenuItem.id = response.data.ID;
					} else {
						// display the error messages.
						var errorMessage = "";
						for (var i = 0; i < response.data.errorList.length; i++) {
							errorMessage += response.data.errorList[i];
						}
						alert(errorMessage);
					}
				}, function() {
					alert("An error has occured");
				});

				
				$scope.menuItems.push(newMenuItem);

				$scope.newItemName = "";
				$scope.newNumberOfServings = "";
				$scope.newItemPrice = "";
				$scope.newNumberOfServings = "";

				$('#addItem').modal('close');
			}
			else{
				alert("Number of servings must greater than or equal to 0");
			}
		}
		else{
			alert("Item name is too long");
		}
	}

	$scope.anOrderAssociated = function(repeat$scope) {
		return true;
	}
	
	$scope.preventNonNumbers = function(value) {
		if (isNaN(parseInt(value[value.length-1])) && value[value.length-1] != '.') {
			return value.substring(0, value.length-1);
		} else {
			return value;
		}
	}
	
	$('.modal').modal();
});


app.controller('ordersController', function($scope, $http, $httpParamSerializer) {
	$scope.pageTitle = "Orders";
	$scope.$parent.ordersActive = "active";
	$scope.orders = [];
	
	$http.get("/ListOrder")
	.then(function(response) {
		if (response.data.errorList.length == 0) {
			//There were no errors.
			
			// passing the json data from the response to the orders
			
			$scope.orders = response.data.orderList;
			alert($scope.orders[0].menuItems);
			
		} else {
			// display the error messages.
			var errorMessage = "";
			for (var i = 0; i < response.data.errorList.length; i++) {
				errorMessage += response.data.errorList[i];
			}
			alert(errorMessage);
		}
	}, function() {
		alert("An error has occured");
	});
	
	$scope.status = "All";
	$scope.filterData = function(order) {
		if($scope.status == 'All') {
			return true;
		} else {
			return order.status == $scope.status;
		}
	};

	$scope.deleteOrder = function(itemIndex, orderNumber) {
		$scope.orders.splice(itemIndex, 1);
		
		var json = {
			"orderNumber": orderNumber
		};
		
		$http.post("/DeleteOrder", $httpParamSerializer(json),
			{
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}
		)
		.then(function(response) {
			if (response.data.errorList.length == 0) {
				//There were no errors.
				alert("Successfully Deleted Order");
			} else {
				// display the error messages.
				var errorMessage = "";
				for (var i = 0; i < response.data.errorList.length; i++) {
					errorMessage += response.data.errorList[i];
				}
				alert(errorMessage);
			}
		}, function() {
			alert("An error has occured");
		});
	};

	$scope.showMenuItems = function(repeat$scope) {
		$scope.orderNumber = repeat$scope.order.orderNumber;
		$scope.orderTimestamp = repeat$scope.order.timeStamp;
		$scope.items = repeat$scope.order.menuItems;
		$scope.totalCost = repeat$scope.order.totalCost;

		$('#orderMenuItems').modal('open');
	}

	$('.modal').modal();
});

app.controller('tablesController', function($scope, $http, $httpParamSerializer) {
	$scope.pageTitle = "Tables";
	$scope.$parent.tablesActive = "active";
	$scope.addSuccess = false;
	
	$scope.tables = [];
	$http.get("/ListTable")
		.then(function(response) {
			if (response.data.errorList.length == 0) {
				//There were no errors.
				
				// passing the json data from the response to the personList
				$scope.tables = response.data.tableList;
			} else {
				// display the error messages.
				var errorMessage = "";
				for (var i = 0; i < response.data.errorList.length; i++) {
					errorMessage += response.data.errorList[i];
				}
				alert(errorMessage);
			}
		}, function() {
			alert("An error has occured");
		});
	
	$scope.status = "All";
	$scope.tableNumber = "";
	$scope.tableCapacity = "";
	$scope.filterData = function(table) {
		if($scope.status == 'All') {
			return table.tableNumber.toString().includes($scope.tableNumber) &&
						table.capacity.toString().includes($scope.tableCapacity);
		} else {
			return table.tableNumber.toString().includes($scope.tableNumber) &&
						table.capacity.toString().includes($scope.tableCapacity) &&
						table.status == $scope.status;
		}
	};
	
	$scope.preventNonNumbers = function(value) {
		if (isNaN(parseInt(value[value.length-1]))) {
			return value.substring(0, value.length-1);
		} else {
			return value;
		}
	}
	
	$scope.createTable = function() {
		if($scope.newTableNumber > 9 && $scope.newTableNumber < 100){
			if($scope.newTableCapacity > 0){
				var newTable = {
						"tableNumber": $scope.newTableNumber,
						"capacity": $scope.newTableCapacity,
						"status": "Vacant"
					};
				
				$http.post("/RegisterTable", $httpParamSerializer(newTable),
						{
						headers: {'Content-Type': 'application/x-www-form-urlencoded'}
						}
					)
					.then(function(response) {
						if (response.data.errorList.length == 0) {
							//There were no errors.
							$scope.addSuccess = true;
							window.scrollTo(0, 0);
						} else {
							// display the error messages.
							var errorMessage = "";
							for (var i = 0; i < response.data.errorList.length; i++) {
								errorMessage += response.data.errorList[i];
							}
							alert(errorMessage);
						}
					}, function() {
						alert("An error has occured");
					});
				
				$scope.tables.push(newTable);

				$scope.newTableNumber = "";
				$scope.newTableCapacity = "";

				$('#addTable').modal('close');
			}
			else{
				alert("Capacity must be greater than 0");
			}
		}
		else{
			alert("Table Number must be exactly 2 numbers");
		}
		
	}
	
	$('.modal').modal();
});