var app = angular.module('myApp', []);
	app.controller('myController', function($scope, $interval, $http) {
	$scope.tableCapacity = "";
	$scope.tables = [];
	
	$http.get("/ListTable")
	.then(function(response) {
		if (response.data.errorList.length == 0) {
			//There were no errors
			// passing the json data from the response to the personList
			$scope.tables = response.data.tableList;
		} else {
			// display the error messages.
			var errorMessage = "";
			for (var i = 0; i < response.data.errorList.length; i++) {
				errorMessage += response.data.errorList[i];
			}
			alert(errorMessage);
		}
	}, function() {
		alert("An error has occured");
	});
	
	$scope.preventNonNumbers = function() {
		if (isNaN(parseInt($scope.tableCapacity[$scope.tableCapacity.length-1]))) {
			$scope.tableCapacity = $scope.tableCapacity.substring(0, $scope.tableCapacity.length-1);
		}
	}
	var tick = function() {
	    $scope.clock = Date.now();
	  }
	  tick();
	  $interval(tick, 1000);
})