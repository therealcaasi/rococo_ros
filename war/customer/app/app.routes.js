app.config(function($routeProvider) {
  $routeProvider
    .when("/", {
      templateUrl: 'app/components/occupy-table/occupyTableView.html'
    })
    .when("/make-order", {
      templateUrl: 'app/components/make-order/makeOrderView.html',
      controller: 'makeOrderController'
    })
    .when("/orders", {
      templateUrl: 'app/components/orders/ordersView.html',
      controller: 'ordersController'
    })
    .when("/tables", {
      templateUrl: 'app/components/tables/tablesView.html',
      controller: 'tablesController'
    });
});