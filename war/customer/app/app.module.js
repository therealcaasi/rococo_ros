var app = angular.module('myApp', ['ngRoute']);
app.controller('myController', function($scope, $routeParams, $interval, $http, $httpParamSerializer) {
	
	
	$scope.tableNumber = getTableNumber();
	
	var json ={
		"tableNumber" : $scope.tableNumber
	};
	alert(json.tableNumber);
	$http.get("/RetrieveTable", $httpParamSerializer(json),
			{
			headers: {'Content-Type': 'application/x-www-form-urlencoded'}
			}
		)
		.then(function(response) {
			if (response.data.errorList.length == 0) {
				//There were no errors.
				alert("Successfully Deleted Item");
			} else {
				// display the error messages.
				var errorMessage = "";
				for (var i = 0; i < response.data.errorList.length; i++) {
					errorMessage += response.data.errorList[i];
				}
				alert(errorMessage);
			}
		}, function() {
			alert("An error has occured");
		});
	 
	
	$scope.tableCapacity = 50;
	var tick = function() {
	    $scope.clock = Date.now();
	  }
	  tick();
	  $interval(tick, 1000);
});

function getTableNumber(){
	var $_GET = {};
	if(document.location.toString().indexOf('?') !== -1) {
	    var query = document.location
	                   .toString()
	                   // get the query string
	                   .replace(/^.*?\?/, '')
	                   // and remove any existing hash string (thanks, @vrijdenker)
	                   .replace(/#.*$/, '')
	                   .split('&');

	    for(var i=0, l=query.length; i<l; i++) {
	       var aux = decodeURIComponent(query[i]).split('=');
	       $_GET[aux[0]] = aux[1];
	    }
	}
	//get the 'index' query parameter
	//alert($_GET['tableNumber']);
	return $_GET['tableNumber'];
}

app.controller('makeOrderController', function($scope, $http, $httpParamSerializer, $filter, $window, $location) {

	$scope.tableNumber = $scope.$parent.tableNumber;
	$scope.menuItems = [];
	$scope.notification = '';

	$http.get("/ListMenuItem").then(function(response) {
		if (response.data.errorList.length == 0) {
			//There were no errors.
			$scope.menuItems = response.data.menuItemList;
		} else {
			// display the error messages.
			var errorMessage = "";
			for (var i = 0; i < response.data.errorList.length; i++) {
				errorMessage += response.data.errorList[i];
			}
			alert(errorMessage);
		}
	}, function() {
		alert("An error has occured");
	});


	$scope.addedMenuItems = [];
	$scope.orderNum = '';
	$scope.status = 'Unpaid';
	$scope.timeStamp = '';
	$scope.itemName = '';
	$scope.availability = 'All';
	$scope.filterData = function(menuItem) {
		if($scope.availability == 'All') {
			return menuItem.itemName.toLowerCase().includes($scope.itemName.toLowerCase());
		} else {
			return menuItem.availability == $scope.availability &&
					menuItem.itemName.toLowerCase().includes($scope.itemName.toLowerCase());
		}
	};

	$scope.total = 0;
	$scope.itemsAdded = 0;
	$scope.orderPayed = false;
	$scope.addItem = function(repeat$scope) {
		addedItem = {
			"ID": repeat$scope.menuItem.id,
			"itemName": repeat$scope.menuItem.itemName,
			"quantity": 1,
			"maxQuantity": repeat$scope.menuItem.numberOfServings,
			"itemPrice": repeat$scope.menuItem.itemPrice,
			"subTotal": repeat$scope.menuItem.itemPrice
		}

		repeat$scope.menuItem.buttonDisabled = true;
		$scope.addedMenuItems.push(addedItem);

		updateTotal(addedItem.subTotal, 1);
		$scope.itemsAdded++;
	}

	$scope.removeItem = function(repeat$scope, itemIndex) {
		for(var i = 0; i < $scope.menuItems.length; i++) {
			if($scope.menuItems[i].id == repeat$scope.addedMenuItem.ID) {
				$scope.menuItems[i].buttonDisabled = false;
				updateTotal(repeat$scope.addedMenuItem.subTotal, 2);
				break;
			}
		}

		$scope.addedMenuItems.splice(itemIndex, 1);
		$scope.itemsAdded--;
	}

	$scope.increaseQuantity = function(repeat$scope) {
		if(repeat$scope.addedMenuItem.quantity < repeat$scope.addedMenuItem.maxQuantity) {
			repeat$scope.addedMenuItem.quantity++;
			repeat$scope.addedMenuItem.subTotal += repeat$scope.addedMenuItem.itemPrice;

			updateTotal(repeat$scope.addedMenuItem.itemPrice, 1);
		}
	}

	$scope.decreaseQuantity = function(repeat$scope, $index) {
		if(repeat$scope.addedMenuItem.quantity > 0) {
			repeat$scope.addedMenuItem.quantity--;
			repeat$scope.addedMenuItem.subTotal -= repeat$scope.addedMenuItem.itemPrice;

			updateTotal(repeat$scope.addedMenuItem.itemPrice, 2);
		}

		if(repeat$scope.addedMenuItem.quantity == 0) {
			$scope.removeItem(repeat$scope, $index);
		}
	}

	$scope.cancelOrder = function(){
		$scope.status="Cancelled";
		$scope.notification = "You have cancelled your order"
		if($scope.orderNum==''){
			createOrder();
		}else{
			updateOrder();
		}
		$window.location.href ="/customer/#!";
		$location.path("/customer/#!");
	}

	function createOrder(){
		var todayWithoutDashes = $filter('date')(new Date(), 'yyyyMMdd');
		var jsonData = {
			"orderNumber": (todayWithoutDashes + String(Math.floor(Math.random()*(999-100+1)+100))),
			"menuItems": JSON.stringify($scope.addedMenuItems),
			"status": $scope.status,
			"timeStamp": String($scope.hours)+":"+String($scope.minutes)+":"+String($scope.seconds),
			"totalCost": $scope.total
		};

		$scope.orderNum=jsonData.orderNumber;
		$scope.timeStamp=jsonData.timeStamp;
		$http.post("/RegisterOrder", $httpParamSerializer(jsonData),
				{
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			)
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					alert($scope.notification);
				} else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}

	function updateOrder(){
		var jsonData = {
				"orderNumber": $scope.orderNum,
				"menuItems": JSON.stringify($scope.addedMenuItems),
				"status": $scope.status,
				"timeStamp": $scope.timeStamp,
				"totalCost": $scope.total
		}

		$http.post("/UpdateOrder", $httpParamSerializer(jsonData),
				{
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				}
			)
			.then(function(response) {
				if (response.data.errorList.length == 0) {
					alert($scope.notification);
				} else {
					var errorMessage = "";
					for (var i = 0; i < response.data.errorList.length; i++) {
						errorMessage += response.data.errorList[i] += "\n";
					}
					alert(errorMessage);
				}
			}, function() {
				alert("An error has occured");
			});
	}


	// Mode 1 = Add
	// Mode 2 = Subtract
	function updateTotal(value, mode) {
		if(mode == 1) {
			$scope.total += value;
		} else {
			$scope.total -= value;
		}
	}

	$scope.noItemOrdered = function() {
		return $scope.itemsAdded == 0;
	}

	$scope.openConfirmModal = function() {
		$('#confirmOrder').modal('open');
	}

	$scope.openPaymentModal = function() {
		$('#payOrder').modal('open');
	}

	var startRedirectCountDown=false;

	$scope.payOrder = function() {
		$scope.orderPayed = true;
		$scope.status="Paid";
		$scope.notification = "Your order has been paid"
		if($scope.orderNum==''){
			createOrder();
		}else{
			updateOrder();
		}
		startRedirectCountDown=true;
		$('#payOrder').modal('close');
	}

	$scope.cancelPayment = function() {
		$('#payOrder').modal('close');
	}

	$('.modal').modal();


	var countDown = 0;
	var countDownRedirect = 10;
	var timer = setInterval(function(){

        var hours   = Math.floor( countDown / 3600);
        var minutes = Math.floor((countDown - (hours * 3600)) / 60);
        var seconds = countDown - (hours * 3600) - (minutes * 60);

        if (hours   < 10) {hours   = "0" + hours;}
        if (minutes < 10) {minutes = "0" + minutes;}
        if (seconds < 10) {seconds = "0" + seconds;}

        $scope.hours = hours;
        $scope.minutes = minutes;
        $scope.seconds = seconds;

        $scope.time=$scope.hours+":"+ $scope.minutes +":"+ $scope.seconds;
        if(startRedirectCountDown==true){
	        if(countDownRedirect<=0){
				$window.location.href = '/customer';
				$location.path( "/customer");
			}
			 countDownRedirect--;
        }

        if($scope.minutes==3){
        	if($scope.orderNum==''){
        		$scope.notification = "Your order has been requested"
    			createOrder();
    		}
        }

        countDown++;
        $scope.$apply();
	 }, 1000);



});