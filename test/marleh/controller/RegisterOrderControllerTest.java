package marleh.controller;

import org.slim3.tester.ControllerTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class RegisterOrderControllerTest extends ControllerTestCase {

    @Test
    public void run() throws Exception {
        tester.start("/RegisterOrder");
        RegisterOrderController controller = tester.getController();
        assertThat(controller, is(notNullValue()));
        assertThat(tester.isRedirect(), is(false));
        assertThat(tester.getDestinationPath(), is("/RegisterOrder.jsp"));
    }
}
