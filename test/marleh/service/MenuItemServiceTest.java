package marleh.service;

import org.slim3.tester.AppEngineTestCase;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class MenuItemServiceTest extends AppEngineTestCase {

    private MenuItemService service = new MenuItemService();

    @Test
    public void test() throws Exception {
        assertThat(service, is(notNullValue()));
    }
}
